# DFL::Login1

DFL Login1 class implements a part of the systemd logind dbus protocol.


### Dependencies:
* <tt>Qt5 Core and DBus (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>
* <tt>logind provider (systemd-logind or elogind, runtime)

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/login1.git dfl-login1`
- Enter the `dfl-login1` folder
  * `cd dfl-login1`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Unable to acquire inhibit locks when used in some application.


### Upcoming
* Any feature that you ask for.
