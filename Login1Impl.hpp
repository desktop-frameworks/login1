/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  DFL Login1 class implements a part of the systemd
 *  logind dbus protocol.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>

typedef struct login1_impl_t {
    /** DBus Login1 Interface */
    QDBusInterface                                           *login1 = nullptr;

    /** DBus Login1 User Session Interface */
    QDBusInterface                                           *session1 = nullptr;

    /** Inhibitor locks */
    int                                                      sleepLockFD     = -1;
    int                                                      shutdownLockFD  = -1;
    int                                                      idleLockFD      = -1;
    int                                                      keyLocksFD      = -1;
    int                                                      lidSwitchLockFD = -1;

    /** App name */
    QString                                                  appName;

    /** Inhibitor locks acquired by this instacne */
    QMap<DFL::Login1::InhibitorLock, DFL::Login1::Inhibitor> mInhibitors;
} Login1Impl;
