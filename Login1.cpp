/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL Login1 class implements a part of the systemd logind dbus protocol.
 **/

#include <fcntl.h>
#include <unistd.h>
#include <QtDBus>

#include "DFLogin1.hpp"
#include "Login1Impl.hpp"

DFL::Login1::Login1( QObject *parent ) : QObject( parent ) {
    impl = new Login1Impl();

    /** MetaTypes registration */
    qRegisterMetaType<DFL::Login1::Inhibitor>( "Inhibitor" );
    qDBusRegisterMetaType<DFL::Login1::Inhibitor>();
    qDBusRegisterMetaType<DFL::Login1::Inhibitors>();

    /** Connect to the login1 interface  */
    impl->login1 = new QDBusInterface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        QDBusConnection::systemBus()
    );

    /** Connect to the login1's session interface  */
    impl->session1 = new QDBusInterface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        QDBusConnection::systemBus()
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        "Lock",
        this,
        SLOT( LockSession() )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        "Unlock",
        this,
        SLOT( UnlockSession() )
    );

    QString orgName( qApp->organizationName() );
    QString appName( qApp->applicationName() );

    if ( orgName.isEmpty() or appName.isEmpty() ) {
        qCritical() << "Unable to start the DBus-based interal service.";
        return;
    }

    impl->appName = appName;
}


DFL::Login1::~Login1() {
    releaseInhibitLocks();

    delete impl->login1;
    delete impl->session1;
}


void DFL::Login1::acquireInhibitLocks( DFL::Login1::InhibitorLocks locks ) {
    if ( locks & DFL::Login1::SleepLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "sleep",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->sleepLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            DFL::Login1::Inhibitor inhibitor {
                .what = "sleep",
                .who  = impl->appName,
                .why  = "Handled by the DE.",
                .mode = "block",
                .uid  = (uint32_t)getuid(),
                .pid  = (uint32_t)getpid(),
            };

            impl->mInhibitors[ DFL::Login1::SleepLock ] = inhibitor;
        }
    }

    if ( locks & DFL::Login1::ShutdownLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "shutdown",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->shutdownLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            DFL::Login1::Inhibitor inhibitor {
                .what = "shutdown",
                .who  = impl->appName,
                .why  = "Handled by the DE.",
                .mode = "block",
                .uid  = (uint32_t)getuid(),
                .pid  = (uint32_t)getpid(),
            };

            impl->mInhibitors[ DFL::Login1::ShutdownLock ] = inhibitor;
        }
    }

    if ( locks & DFL::Login1::IdleLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "idle",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->idleLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            DFL::Login1::Inhibitor inhibitor {
                .what = "idle",
                .who  = impl->appName,
                .why  = "Handled by the DE.",
                .mode = "block",
                .uid  = (uint32_t)getuid(),
                .pid  = (uint32_t)getpid(),
            };

            impl->mInhibitors[ DFL::Login1::IdleLock ] = inhibitor;
        }
    }

    QStringList keyLockNames;

    keyLockNames << (locks & DFL::Login1::PowerKeyLock ? "handle-power-key" : "");
    keyLockNames << (locks & DFL::Login1::SuspendKeyLock ? "handle-suspend-key" : "");
    keyLockNames << (locks & DFL::Login1::HibernateKeyLock ? "handle-hibernate-key" : "");

    if ( keyLockNames.length() ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            keyLockNames.join( ":" ),
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->keyLocksFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            DFL::Login1::Inhibitor inhibitor {
                .what = keyLockNames.join( ":" ),
                .who  = impl->appName,
                .why  = "Handled by the DE.",
                .mode = "block",
                .uid  = (uint32_t)getuid(),
                .pid  = (uint32_t)getpid(),
            };

            if ( locks & DFL::Login1::PowerKeyLock ) {
                impl->mInhibitors[ DFL::Login1::PowerKeyLock ] = inhibitor;
            }

            if ( locks & DFL::Login1::SuspendKeyLock ) {
                impl->mInhibitors[ DFL::Login1::SuspendKeyLock ] = inhibitor;
            }

            if ( locks & DFL::Login1::HibernateKeyLock ) {
                impl->mInhibitors[ DFL::Login1::HibernateKeyLock ] = inhibitor;
            }
        }
    }

    if ( locks & DFL::Login1::LidSwitchLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "handle-lid-switch",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->lidSwitchLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            DFL::Login1::Inhibitor inhibitor {
                .what = "handle-lid-switch",
                .who  = impl->appName,
                .why  = "Handled by the DE.",
                .mode = "block",
                .uid  = (uint32_t)getuid(),
                .pid  = (uint32_t)getpid(),
            };

            impl->mInhibitors[ DFL::Login1::LidSwitchLock ] = inhibitor;
        }
    }

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForSleep",
        this,
        SLOT( PrepareForSleep( bool ) )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForShutdown",
        this,
        SLOT( PrepareForShutdown( bool ) )
    );
}


void DFL::Login1::releaseInhibitLocks() {
    close( impl->sleepLockFD );
    close( impl->shutdownLockFD );
    close( impl->idleLockFD );
    close( impl->keyLocksFD );
    close( impl->lidSwitchLockFD );

    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForSleep",
        this,
        SLOT( PrepareForSleep( bool ) )
    );

    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForShutdown",
        this,
        SLOT( PrepareForShutdown( bool ) )
    );
}


DFL::Login1::Inhibitors DFL::Login1::acquiredInhibitors() {
    return impl->mInhibitors.values();
}


DFL::Login1::Inhibitors DFL::Login1::listInhibitors() {
    QDBusReply<DFL::Login1::Inhibitors> reply = impl->login1->call( "ListInhibitors" );

    return reply.value();
}


bool DFL::Login1::setBrightness( double brightness ) {
    QDirIterator dirIt( "/sys/class/backlight", QDir::Dirs | QDir::NoDotAndDotDot );

    int failed = 0;

    while ( dirIt.hasNext() ) {
        /** Get the next entry */
        dirIt.next();

        /** Get the info corresponding to that entry  */
        QFileInfo info( dirIt.fileInfo() );

        /** Read the maximum brightness */
        QFile max( dirIt.filePath() + "/max_brightness" );
        max.open( QFile::ReadOnly );
        int maxVal = max.readAll().toInt();
        max.close();

        if ( maxVal == 0 ) {
            maxVal = 100;
        }

        QDBusReply<void> reply = impl->session1->call( "SetBrightness", "backlight", dirIt.fileName(), ( uint )(brightness * maxVal / 100.0) );

        if ( reply.isValid() == false ) {
            qWarning() << "Error changing brightness of " + dirIt.fileName() + " :" << reply.error().message();
            failed++;
        }
    }

    return (failed ? false : true);
}


bool DFL::Login1::setBrightness( QString driver, QString device, uint brightness ) {
    QDBusReply<void> reply = impl->session1->call( "SetBrightness", driver, device, brightness );

    if ( not reply.isValid() ) {
        qWarning() << "Error changing brightness:" << reply.error().message();
        return false;
    }

    return true;
}


int DFL::Login1::request( QString request ) {
    if ( request.startsWith( "Can" ) ) {
        QDBusReply<QString> reply = impl->login1->call( request );

        if ( not reply.isValid() ) {
            return -3;
        }

        if ( reply.value() == "na" ) {
            return -2;
        }

        else if ( reply.value() == "challenge" ) {
            return -1;
        }

        else if ( reply.value() == "no" ) {
            return 0;
        }

        else {
            return 1;
        }
    }

    else {
        /** We need to release the correct lock. */
        if ( (request == "PowerOff") or (request == "Reboot") ) {
            close( impl->shutdownLockFD );
            impl->shutdownLockFD = -1;
        }

        else {
            close( impl->sleepLockFD );
            impl->sleepLockFD = -1;
        }

        /**
         * Authorization may be needed to perform this operation.
         * Or others may have taken delay/block locks.
         * The DE has taken care of inhibit locks (hopefully).
         * So we can force the locks, or allow PKit to perform
         * authorization.
         */
        QDBusReply<void> reply = impl->login1->call( request, true );

        if ( not reply.isValid() ) {
            return 0;
        }

        else {
            return 1;
        }
    }
}


int DFL::Login1::schedulePowerEvent( QString type, qulonglong usec ) {
    if ( (type != "poweroff") and (type != "reboot") and (type != "cancel") ) {
        return 0;
    }

    if ( type == "cancel" ) {
        /** Cancel the scheduled shutdown */
        QDBusReply<bool> cancelReply = impl->login1->call( "CancelScheduledShutdown" );

        /** Re-acquire inhibit lock */
        QDBusReply<QDBusUnixFileDescriptor> lockReply = impl->login1->call(
            "Inhibit",
            "shutdown",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( lockReply.isValid() ) {
            impl->shutdownLockFD = fcntl( lockReply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }

        if ( cancelReply.isValid() ) {
            /**
             * Return 0 on failed, and 1 on cancellation
             */
            return cancelReply.value();
        }

        else {
            return 0;
        }
    }

    else {
        if ( (uint32_t)QDateTime::currentDateTime().toMSecsSinceEpoch() * 1000 > usec ) {
            return 2;
        }

        /** Close the inhibit locks */
        close( impl->shutdownLockFD );
        impl->shutdownLockFD = -1;

        /** Perform the dbus call to schedule the request */
        QDBusReply<void> reply = impl->login1->call( "ScheduleShutdown", type, usec );

        /** Failed: re-acquire the shutdown lock */
        if ( not reply.isValid() ) {
            /** Re-acquire inhibit lock */
            QDBusReply<QDBusUnixFileDescriptor> lockReply = impl->login1->call(
                "Inhibit",
                "shutdown",
                impl->appName,
                "Handled by the DE.",
                "block"
            );

            if ( lockReply.isValid() ) {
                impl->shutdownLockFD = fcntl( lockReply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            }

            return 0;
        }

        /** Okay, scheduled */
        else {
            return 1;
        }
    }
}


int DFL::Login1::schedulePowerEventIn( QString type, qulonglong usec ) {
    if ( type == "cancel" ) {
        return schedulePowerEvent( type, 0 );
    }

    if ( usec > 0 ) {
        qulonglong usecs = QDateTime::currentDateTime().toMSecsSinceEpoch() * 1000 + usec;
        return schedulePowerEvent( type, usecs );
    }

    else {
        return 2;
    }

    return 0;
}


int DFL::Login1::schedulePowerEventAt( QString type, QDateTime dt ) {
    if ( type == "cancel" ) {
        return schedulePowerEvent( type, 0 );
    }

    if ( QDateTime::currentDateTime() <= dt ) {
        return schedulePowerEvent( type, dt.toMSecsSinceEpoch() * 1000 );
    }

    else {
        return 2;
    }

    return 0;
}


void DFL::Login1::PrepareForSleep( bool active ) {
    /** Someone requested Sleep; such requests are ignored */
    if ( active ) {
        //
    }

    /** We're coming out of Sleep (or failed); re-acquire the lock */
    else {
        if ( impl->sleepLockFD == -1 ) {
            QDBusReply<QDBusUnixFileDescriptor> reply;
            reply = impl->login1->call( "Inhibit", "sleep", impl->appName, "Handled by the DE.", "block" );

            if ( reply.isValid() ) {
                impl->sleepLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            }
        }
    }
}


void DFL::Login1::PrepareForShutdown( bool active ) {
    /** Someone requested Shutdown; such requests are ignored */
    if ( active ) {
        //
    }

    /** Shutdown failed; re-acquire the lock */
    else {
        if ( impl->shutdownLockFD == -1 ) {
            QDBusReply<QDBusUnixFileDescriptor> reply;
            reply = impl->login1->call( "Inhibit", "shutdown", impl->appName, "Handled by the DE.", "block" );

            if ( reply.isValid() ) {
                impl->shutdownLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            }
        }
    }
}


void DFL::Login1::LockSession() {
    qDebug() << "ScreenLockRequested";
}


void DFL::Login1::UnlockSession() {
    qDebug() << "ScreenUnlockRequested";
}


QDBusArgument &operator<<( QDBusArgument& arg, const DFL::Login1::Inhibitor& inhibitor ) {
    arg.beginStructure();
    arg << inhibitor.what;
    arg << inhibitor.who;
    arg << inhibitor.why;
    arg << inhibitor.mode;
    arg << inhibitor.uid;
    arg << inhibitor.pid;
    arg.endStructure();
    return arg;
}


const QDBusArgument &operator>>( const QDBusArgument& arg, DFL::Login1::Inhibitor& inhibitor ) {
    arg.beginStructure();
    arg >> inhibitor.what;
    arg >> inhibitor.who;
    arg >> inhibitor.why;
    arg >> inhibitor.mode;
    arg >> inhibitor.uid;
    arg >> inhibitor.pid;
    arg.endStructure();
    return arg;
}
